package com.lambda.playaround;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    TextView tv;
    Button bt, bt2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv = findViewById(R.id.textView);
        bt = findViewById(R.id.button);
        bt2 = findViewById(R.id.button2);

        tv.setText("WELCOME TO WORKSHOP!");

        bt.setOnClickListener(this);
        bt2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.button: tv.setText("Hello THERE!!!");
            break;
            case R.id.button2: tv.setText("Danke!!");
            break;
        }
    }
}
